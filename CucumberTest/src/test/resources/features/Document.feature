@InitWeb
Feature: Operations on a Document

  @InProgress
  Scenario: Creating documents
    Given I am on Start Page
    And that there exists a document template with name 'template Name'
    When creating a Document with name 'name' and content 'content' and with template with name 'template Name'
    Then a document should be created with name 'name' and content 'content' and with template with name 'template Name'