@InitWeb
Feature: Create Read Update Delete Operations on a Document Template

  @Completed
  Scenario: This scenario is for creating a new document template
    Given I am on Start Page
    When creating a Document Template with name 'template Name' 
    Then a Document Template with name 'template Name' should exist
    
  @Completed    
  Scenario Outline: This scenario is for creating new document templates using Examples
    Given I am on Start Page
    When creating a Document Template with name '<template>'
    Then a Document Template with name '<template>' should exist
  Examples:
	|template|
	|template name|
	|other template name|
	
  @Completed 
  Scenario: This scenario is for listing existing document templates 
	Given the following document templates exists:
		|template name|
		|other template name|
	When listing Document Templates
	Then the following Document Templates should be listed:
		|template name|
		|other template name|
		
  @Completed 
  Scenario: This scenario is for updating existing document templates 
	Given that there exists a document template with name 'template Name Before'
	When updating a document template with name 'template Name Before' to name 'template Name After'
	Then a Document Template with name 'template Name After' should exist
	And a Document Template with name 'template Name Before' should not exist

  @Completed
  Scenario: This scenario is for updating existing document templates 
	Given that there exists a document template with name 'template to be Deleted'
	When deleting a document template with name 'template to be Deleted'
	And a Document Template with name 'template to be Deleted' should not exist