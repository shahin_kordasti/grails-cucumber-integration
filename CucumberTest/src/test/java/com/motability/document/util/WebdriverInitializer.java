package com.motability.document.util;

import org.openqa.selenium.WebDriver;

/**
 * This class is used for initializing Selenium Webdriver with whatever drivers that are needed. It has to be loaded
 * before a {@link WebDriver} instance is created so this can either be done manually or as a dependency in the Spring
 * beans configuration
 */
public class WebdriverInitializer {

    public WebdriverInitializer() {
	setChromeSeleniumDriver();
    }

    public void unset() {
	unsetChromeSeleniumDriver();
    }

    private void unsetChromeSeleniumDriver() {
	System.clearProperty("webdriver.chrome.driver");
    }

    private void setChromeSeleniumDriver() {
	String driverPath = WebdriverInitializer.class.getResource("/drivers/chrome/chromedriver.exe").getPath();
	System.setProperty("webdriver.chrome.driver", driverPath);
    }
}
