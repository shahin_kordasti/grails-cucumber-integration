package com.motability.document.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.motability.document.pageobjects.doctemplate.DocumentTemplateListPage;

public class StartPage extends CommonPage{

    @FindBy(how = How.ID, using="grailsLogo")
    private WebElement logo;
    
    @FindBy(how = How.LINK_TEXT, using="com.motability.DocumentTemplateController")
    private WebElement documentTemplateListLink;
    
    public StartPage(WebDriver webDriver){
	super(webDriver);
    }

    public boolean isLoaded() {
	return logo.isDisplayed();
    }

    public DocumentTemplateListPage openDocumentTemplatePage() {
	documentTemplateListLink.click();
	return PageFactory.initElements(webDriver, DocumentTemplateListPage.class);
    }
}
