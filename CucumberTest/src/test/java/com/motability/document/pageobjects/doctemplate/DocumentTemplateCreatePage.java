package com.motability.document.pageobjects.doctemplate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.motability.document.pageobjects.CommonPage;

public class DocumentTemplateCreatePage extends CommonPage {
    @FindBy(how = How.ID, using = "name")
    private WebElement nameField;

    @FindBy(how = How.ID, using = "create")
    private WebElement createButton;
    
    public DocumentTemplateCreatePage(WebDriver webDriver) {
	super(webDriver);
    }

    public DocumentTemplateShowPage createDocumentTemplate(String templateName) {
	nameField.sendKeys(templateName);
	createButton.click();
	return PageFactory.initElements(webDriver, DocumentTemplateShowPage.class);
    }
}
