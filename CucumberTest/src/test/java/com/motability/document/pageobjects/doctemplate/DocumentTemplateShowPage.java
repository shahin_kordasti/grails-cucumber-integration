package com.motability.document.pageobjects.doctemplate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.motability.document.pageobjects.CommonPage;

public class DocumentTemplateShowPage extends CommonPage {

    @FindBy(how = How.CLASS_NAME, using = "message")
    private WebElement changedMessageBox;

    @FindBy(how = How.ID, using = "show-documentTemplate")
    private WebElement showDocumentTemplateBox;

    @FindBy(how = How.CLASS_NAME, using = "list")
    private WebElement listDocumentTemplatePage;

    @FindBy(how = How.CLASS_NAME, using = "edit")
    private WebElement editDocumentTemplateButton;

    @FindBy(how = How.CLASS_NAME, using = "delete")
    private WebElement deleteDocumentTemplateButton;

    public DocumentTemplateShowPage(WebDriver webDriver) {
	super(webDriver);
    }

    public boolean hasCreatedDocumentTemplate() {
	return changedMessageBox != null && changedMessageBox.getText().contains("created");
    }

    public boolean isDisplayed() {
	return showDocumentTemplateBox.isDisplayed();
    }

    public DocumentTemplateListPage openListDocumentTemplatesPage() {
	listDocumentTemplatePage.click();
	return PageFactory.initElements(webDriver, DocumentTemplateListPage.class);
    }

    public DocumentTemplateEditPage openEditDocumentTemplatePage() {
	editDocumentTemplateButton.click();
	return PageFactory.initElements(webDriver, DocumentTemplateEditPage.class);
    }

    public DocumentTemplateListPage deleteDocumentTemplate(String templateName) {
	deleteDocumentTemplateButton.click();
	webDriver.switchTo().alert().accept();
	return PageFactory.initElements(webDriver, DocumentTemplateListPage.class);
    }
}
