package com.motability.document.pageobjects.doctemplate;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.motability.document.pageobjects.CommonPage;

public class DocumentTemplateEditPage extends CommonPage {
    @FindBy(how = How.ID, using = "name")
    private WebElement nameInputField;

    @FindBy(how = How.CLASS_NAME, using = "save")
    private WebElement updateButton;

    public DocumentTemplateEditPage(WebDriver webDriver) {
	super(webDriver);
    }

    public DocumentTemplateShowPage updateName(String templateAfter) {
	nameInputField.clear();
	nameInputField.sendKeys(templateAfter);
	updateButton.click();
	return PageFactory.initElements(webDriver, DocumentTemplateShowPage.class);
    }
}
