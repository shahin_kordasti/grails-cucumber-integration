package com.motability.document.pageobjects;

import org.openqa.selenium.WebDriver;

/**
 * Super class for all page objects
 */
public class CommonPage {

    protected WebDriver webDriver;

    public CommonPage(WebDriver webDriver) {
	this.webDriver = webDriver;
    }
}
