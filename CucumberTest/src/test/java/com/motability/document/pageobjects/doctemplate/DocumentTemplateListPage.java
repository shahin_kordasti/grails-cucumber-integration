package com.motability.document.pageobjects.doctemplate;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.motability.document.pageobjects.CommonPage;

public class DocumentTemplateListPage extends CommonPage {
    @FindBy(how = How.ID, using = "list-documentTemplate")
    private WebElement listBoxElement;

    @FindBy(how = How.LINK_TEXT, using = "New DocumentTemplate")
    private WebElement newDocumentTemplateButton;

    @FindBy(how = How.ID, using = "list-documentTemplate-table-body")
    private WebElement documentTemplateListTableBody;

    public DocumentTemplateListPage(WebDriver webDriver) {
	super(webDriver);
    }

    public boolean isDisplayed() {
	return listBoxElement.isDisplayed();
    }

    public DocumentTemplateCreatePage openNewDocumentTemplatePage() {
	newDocumentTemplateButton.click();
	return PageFactory.initElements(webDriver, DocumentTemplateCreatePage.class);
    }

    public boolean documentTemplateWithNameExists(String templateName) {
	WebElement documentTemplateTableElement = getTemplateLinkElement(templateName, false);
	return documentTemplateTableElement != null && documentTemplateTableElement.isDisplayed();
    }

    private WebElement getTemplateLinkElement(String templateName, boolean throwNoSuchElementException) {
	try {
	    return documentTemplateListTableBody.findElement(By.linkText(templateName));
	}
	catch (NoSuchElementException e) {
	    // TODO: log exception
	    e.printStackTrace();
	    if (throwNoSuchElementException) {
		throw e;
	    } else {
		return null;
	    }
	}
    }

    public DocumentTemplateShowPage openShowDocumentTemplatePage(String templateName) {
	WebElement documentTemplateTableElement = getTemplateLinkElement(templateName, true);
	documentTemplateTableElement.click();
	return PageFactory.initElements(webDriver, DocumentTemplateShowPage.class);
    }
}
