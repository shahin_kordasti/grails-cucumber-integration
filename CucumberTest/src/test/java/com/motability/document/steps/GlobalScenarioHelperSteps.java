package com.motability.document.steps;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.motability.document.pageobjects.StartPage;

import cucumber.api.java.After;
import cucumber.api.java.Before;

/**
 * This helper class exists mainly because Cucumber JVM does not allow Step classes to extend from other classes which
 * define hooks.
 */
public class GlobalScenarioHelperSteps {

    @Autowired
    private WebDriver webDriver;

    @Value("${target.app.base.url}")
    private String baseApplicationUrl;

    @Before("@InitWeb")
    public void beforeScenario() {
	webDriver.get(baseApplicationUrl);
    }
    
    @After("@InitWeb")
    public void afterScenario() {
	//TODO cleanup database...
    }

    public StartPage getStartPage() {
	StartPage startPage = PageFactory.initElements(webDriver, StartPage.class);
	return startPage;
    }

}
