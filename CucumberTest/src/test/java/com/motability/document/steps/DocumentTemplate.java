package com.motability.document.steps;

public class DocumentTemplate {
    
    private String templateName;
    
    public DocumentTemplate(String templateName){
	this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }
}
