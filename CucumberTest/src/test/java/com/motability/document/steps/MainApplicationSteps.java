package com.motability.document.steps;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import cucumber.api.java.en.Given;

public class MainApplicationSteps {

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private GlobalScenarioHelperSteps globalScenario;

    @Given("^I am on Start Page$")
    public void i_am_on_Start_Page() throws Throwable {
	assertTrue(globalScenario.getStartPage().isLoaded());
    }
}
