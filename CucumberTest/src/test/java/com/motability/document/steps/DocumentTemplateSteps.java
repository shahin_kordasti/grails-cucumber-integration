package com.motability.document.steps;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.motability.document.pageobjects.StartPage;
import com.motability.document.pageobjects.doctemplate.DocumentTemplateCreatePage;
import com.motability.document.pageobjects.doctemplate.DocumentTemplateEditPage;
import com.motability.document.pageobjects.doctemplate.DocumentTemplateListPage;
import com.motability.document.pageobjects.doctemplate.DocumentTemplateShowPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class DocumentTemplateSteps {

    private DocumentTemplateShowPage showPage;

    @Autowired
    private GlobalScenarioHelperSteps globalScenario;

    private DocumentTemplateListPage listPage;

   @When("^creating a Document Template with name '(.*)'$")
    public void creating_a_Document_Template(String templateName) throws Throwable {
	StartPage startPage = globalScenario.getStartPage();
	DocumentTemplateListPage listPage = startPage.openDocumentTemplatePage();
	assertTrue("The Document Template Listing Page was not properly loaded", listPage.isDisplayed());

	createDocumentTemplate(templateName, listPage);

	assertTrue("Document show page was not displayed", showPage.isDisplayed());
	assertTrue("Document created message was not displayed", showPage.hasCreatedDocumentTemplate());
    }

    private void createDocumentTemplate(String templateName, DocumentTemplateListPage listPage) {
	DocumentTemplateCreatePage createPage = listPage.openNewDocumentTemplatePage();
	showPage = createPage.createDocumentTemplate(templateName);
    }

    @Then("^a Document Template with name '(.*)' should exist$")
    public void a_Document_Template_should_exist(String templateName) throws Throwable {
	assertTrue("Was not at Show document template page", showPage != null && showPage.isDisplayed());
	listPage = showPage.openListDocumentTemplatesPage();
	templateExists(templateName, listPage);
    }

    private void templateExists(String templateName, DocumentTemplateListPage docListPage) {
	assertTrue("Document Template with name " + templateName + " not found",
		   docListPage.documentTemplateWithNameExists(templateName));
    }

    @Given("^the following document templates exists:$")
    public void the_following_document_templates_exists(List<DocumentTemplate> templates) throws Throwable {
	StartPage startPage = globalScenario.getStartPage();
	DocumentTemplateListPage listPage = startPage.openDocumentTemplatePage();

	for (DocumentTemplate documentTemplate : templates) {
	    createDocumentTemplate(documentTemplate.getTemplateName(), listPage);
	}
    }

    @When("^listing Document Templates$")
    public void listing_Document_Templates() throws Throwable {
	assertTrue("Was not at Show document template page", showPage != null && showPage.isDisplayed());
	listPage = showPage.openListDocumentTemplatesPage();
	assertTrue("The Document Template Listing Page was not properly loaded", listPage.isDisplayed());
    }

    @Then("^the following Document Templates should be listed:$")
    public void the_following_Document_Templates_should_be_listed(List<DocumentTemplate> templates) throws Throwable {
	assertTrue("Was not at list document template page", listPage != null && listPage.isDisplayed());
	for (DocumentTemplate docTemplate : templates) {
	    templateExists(docTemplate.getTemplateName(), listPage);
	}
    }

    @Given("^that there exists a document template with name '(.*)'$")
    public void that_there_exists_a_document_template_with_name(String templateName) throws Throwable {
	StartPage startPage = globalScenario.getStartPage();
	DocumentTemplateListPage listPage = startPage.openDocumentTemplatePage();
	createDocumentTemplate(templateName, listPage);
    }

    @When("^updating a document template with name '(.*)' to name '(.*)'$")
    public void updating_a_document_template_with_name_before_to_name_After(String templateBefore, String templateAfter)
															throws Throwable {
	assertTrue("Was not at Show document template page", showPage != null && showPage.isDisplayed());
	DocumentTemplateListPage listPage = showPage.openListDocumentTemplatesPage();
	DocumentTemplateEditPage editPage =
					    listPage.openShowDocumentTemplatePage(templateBefore)
						    .openEditDocumentTemplatePage();
	showPage = editPage.updateName(templateAfter);
    }

    @Then("^a Document Template with name '(.*)' should not exist$")
    public void a_Document_Template_with_name_should_not_exist(String templateName) throws Throwable {
	assertTrue("Was not at list document template page", listPage != null && listPage.isDisplayed());
	assertFalse("Document Template with name " + templateName + " found but should not have been",
		    listPage.documentTemplateWithNameExists(templateName));
    }

    @When("^deleting a document template with name '(.*)'$")
    public void deleting_a_document_template_with_name(String templateName) throws Throwable {
	assertTrue("Was not at Show document template page", showPage != null && showPage.isDisplayed());
	listPage = showPage.openListDocumentTemplatesPage();

	listPage = listPage.openShowDocumentTemplatePage(templateName).deleteDocumentTemplate(templateName);
    }

}