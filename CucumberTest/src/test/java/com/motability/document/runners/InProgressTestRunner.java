package com.motability.document.runners;

import org.junit.runner.RunWith;

import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@Cucumber.Options(features = { "classpath:features" },
		  glue = "com.motability.document.steps",
		  tags = { "@InProgress" },
		  format = { "html:target/cucumber-html-report", "json-pretty:target/cucumber-report.json" })
public class InProgressTestRunner {

}
