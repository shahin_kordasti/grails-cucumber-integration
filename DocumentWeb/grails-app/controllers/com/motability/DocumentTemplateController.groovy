package com.motability

import org.springframework.dao.DataIntegrityViolationException

class DocumentTemplateController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 100, 100)
        [documentTemplateInstanceList: DocumentTemplate.list(params), documentTemplateInstanceTotal: DocumentTemplate.count()]
    }

    def create() {
        [documentTemplateInstance: new DocumentTemplate(params)]
    }

    def save() {
        def documentTemplateInstance = new DocumentTemplate(params)
        if (!documentTemplateInstance.save(flush: true)) {
            render(view: "create", model: [documentTemplateInstance: documentTemplateInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), documentTemplateInstance.id])
        redirect(action: "show", id: documentTemplateInstance.id)
    }

    def show(Long id) {
        def documentTemplateInstance = DocumentTemplate.get(id)
        if (!documentTemplateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), id])
            redirect(action: "list")
            return
        }

        [documentTemplateInstance: documentTemplateInstance]
    }

    def edit(Long id) {
        def documentTemplateInstance = DocumentTemplate.get(id)
        if (!documentTemplateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), id])
            redirect(action: "list")
            return
        }

        [documentTemplateInstance: documentTemplateInstance]
    }

    def update(Long id, Long version) {
        def documentTemplateInstance = DocumentTemplate.get(id)
        if (!documentTemplateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (documentTemplateInstance.version > version) {
                documentTemplateInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'documentTemplate.label', default: 'DocumentTemplate')] as Object[],
                          "Another user has updated this DocumentTemplate while you were editing")
                render(view: "edit", model: [documentTemplateInstance: documentTemplateInstance])
                return
            }
        }

        documentTemplateInstance.properties = params

        if (!documentTemplateInstance.save(flush: true)) {
            render(view: "edit", model: [documentTemplateInstance: documentTemplateInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), documentTemplateInstance.id])
        redirect(action: "show", id: documentTemplateInstance.id)
    }

    def delete(Long id) {
        def documentTemplateInstance = DocumentTemplate.get(id)
        if (!documentTemplateInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), id])
            redirect(action: "list")
            return
        }

        try {
            documentTemplateInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'documentTemplate.label', default: 'DocumentTemplate'), id])
            redirect(action: "show", id: id)
        }
    }
}
