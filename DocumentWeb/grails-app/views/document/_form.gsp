<%@ page import="com.motability.Document" %>



<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="document.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${documentInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'content', 'error')} ">
	<label for="content">
		<g:message code="document.content.label" default="Content" />
		
	</label>
	<g:textField name="content" value="${documentInstance?.content}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: documentInstance, field: 'template', 'error')} required">
	<label for="template">
		<g:message code="document.template.label" default="Template" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="template" name="template.id" from="${com.motability.DocumentTemplate.list()}" optionKey="id" required="" value="${documentInstance?.template?.id}" class="many-to-one"/>
</div>

