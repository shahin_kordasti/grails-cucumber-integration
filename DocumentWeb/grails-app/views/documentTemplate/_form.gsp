<%@ page import="com.motability.DocumentTemplate" %>



<div class="fieldcontain ${hasErrors(bean: documentTemplateInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="documentTemplate.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${documentTemplateInstance?.name}"/>
</div>

