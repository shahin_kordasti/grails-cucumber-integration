package com.motability

class DocumentTemplate {
    String name
    static hasMany = [docments:Document]
    static constraints = {
        name blank:false
    }
}
