package com.motability

class Document {
    String name
    String content
    DocumentTemplate template

    static constraints = {
        name blank: false
        content blank: true
        template blank: false
    }
}
